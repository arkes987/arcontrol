﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arcontrol.Model
{
    public class Cmd
    {
        #region Static Methods
        public static void Post(string message, string type)
        {
            switch(type)
            {
                case "error":
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case "done":
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }
            if(!String.IsNullOrEmpty(message))
                Console.WriteLine(DateTime.UtcNow + " : " + message);

            Console.ResetColor();
        }
        
        public static void Post(Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(DateTime.UtcNow + " : " + e.ToString());
            Console.ResetColor();
        }
        #endregion
    }
}
