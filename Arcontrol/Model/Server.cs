﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Arcontrol.Model
{
    enum Command
    {
        Login,      //Log into the server
        Logout,     //Logout of the server
        Message,    //Send a text message to all the chat clients
        Null        //No command
    }
    public class Server
    {
        /// <summary>
        /// Constructor to setup basic variables and run listener
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public Server(string Ip, UInt16 Port)
        {
            IpAdress = Ip;
            this.Port = Port;
            ByteData = new byte[1024];
            ClientList = new ArrayList();
            Setup();
        }

        struct ClientInfo
        {
            public Socket Socket;   //Socket of the client
            public string StrName;  //Name by which the user logged into the chat room
        }

        /// <summary>
        /// Properties for basic server setup
        /// </summary>
        #region Properties
        private Socket _Listener;
        private string _Ipadress;
        private UInt16 _Port;
        private byte[] _ByteData;
        private ArrayList _ClientList;
        private bool _ServerEnabled;

        public Socket Listener
        {
            get { return _Listener; }
            set { _Listener = value; }
        }

        public ArrayList ClientList
        {
            get { return _ClientList; }
            set { _ClientList = value; }
        }

        public string IpAdress
        {
            get { return _Ipadress; }
            set { _Ipadress = value; }
        }

        public UInt16 Port
        {
            get { return _Port; }
            set { _Port = value; }
        }

        public byte[] ByteData
        {
            get { return _ByteData; }
            set { _ByteData = value; }
        }

        public bool ServerEnabled
        {
            get { return _ServerEnabled; }
            set { _ServerEnabled = value; }
        }


        #endregion


        #region Methods
        /// <summary>
        /// Setup's our server & start waiting for clients
        /// </summary>
        private void Setup()
        {
            Listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(IpAdress), Port);
            Listener.Bind(ipEndPoint);
            Listener.Listen(4);
            Listener.BeginAccept(new AsyncCallback(AcceptClients), null);
            Cmd.Post("New server created", "done");
        }

        /// <summary>
        /// Accept client and start listening for new clients
        /// </summary>
        /// <param name="arg"></param>
        private void AcceptClients(IAsyncResult arg)
        {
            if (ServerEnabled)
            {
                try
                {
                    Socket Client = Listener.EndAccept(arg); // finish listening for socket
                    Listener.BeginAccept(new AsyncCallback(AcceptClients), null); // starts listening for new clents
                    Listener.BeginReceive(ByteData, 0, ByteData.Length, SocketFlags.None, new AsyncCallback(RecieveData), Listener);

                }
                catch (Exception e)
                {
                    Cmd.Post(e);
                }
            }
        }

        /// <summary>
        /// Actions on recieve any data from clients
        /// </summary>
        /// <param name="arg"></param>
        private void RecieveData(IAsyncResult arg)
        {
            try
            {
                Socket ClientSocket = (Socket)arg.AsyncState;
                ClientSocket.EndReceive(arg);
                Data MsgRecieved = new Data(ByteData);
                Data MsgToSend = new Data();
                MsgToSend.CmdCommand = MsgRecieved.CmdCommand;
                MsgToSend.StrName = MsgRecieved.StrName;

                switch (MsgRecieved.CmdCommand)
                {
                    case Command.Login:
                        ClientInfo clientInfo = new ClientInfo();
                        clientInfo.Socket = ClientSocket;
                        clientInfo.StrName = MsgRecieved.StrName;
                        ClientList.Add(clientInfo);
                        MsgToSend.StrMessage = MsgRecieved.StrName + " joined to server.";
                        break;

                    case Command.Message:
                        MsgToSend.StrMessage = MsgRecieved.StrName + " : " + MsgRecieved.StrMessage;
                        break;

                    case Command.Logout:
                        uint Index = 0;
                        foreach (ClientInfo client in ClientList)
                        {
                            if (client.Socket == ClientSocket)
                            {
                                ClientList.RemoveAt((int)Index);
                                break;
                            }
                            Index++;
                        }
                        break;
                }

                if (MsgRecieved.CmdCommand != Command.Logout)
                    ClientSocket.BeginReceive(ByteData, 0, ByteData.Length, SocketFlags.None, new AsyncCallback(RecieveData), ClientSocket);

            }
            catch (Exception e)
            {
                Cmd.Post(e);
            }
        }

        public UInt16 ClientCount()
        {
            return (UInt16)ClientList.Count;
        }


        #endregion
    }

    class Data
    {
        public Data()
        {
            this.CmdCommand = Command.Null;
            this.StrMessage = null;
            this.StrName = null;
        }

        private string _StrName;      //Name by which the client logs into the room
        private string _StrMessage;   //Message text
        private Command _CmdCommand;  //Command type (login, logout, send message, etcetera)

        #region Properties
        public string StrName
        {
            get { return _StrName; }
            set { _StrName = value; }
        }
        public string StrMessage
        {
            get { return _StrMessage; }
            set { _StrMessage = value; }
        }
        public Command CmdCommand
        {
            get { return _CmdCommand; }
            set { _CmdCommand = value; }
        }
        #endregion


        #region Methods
        /// <summary>
        /// Converts the bytes into an object of type Data
        /// </summary>
        /// <param name="data"></param>
        public Data(byte[] data)
        {
            this.CmdCommand = (Command)BitConverter.ToInt32(data, 0);
            int NameLen = BitConverter.ToInt32(data, 4);
            int MsgLen = BitConverter.ToInt32(data, 8);

            if (NameLen > 0)
                this.StrName = Encoding.UTF8.GetString(data, 12, NameLen);
            else
                this.StrName = null;

            if (MsgLen > 0)
                this.StrMessage = Encoding.UTF8.GetString(data, 12 + NameLen, MsgLen);
            else
                this.StrMessage = null;
        }

        /// <summary>
        /// Convert Data structure into an array of bytes
        /// </summary>
        /// <returns></returns>
        public byte[] ToByte()
        {
            List<byte> Result = new List<byte>();

            Result.AddRange(BitConverter.GetBytes((int)CmdCommand));

            if (StrName != null)
                Result.AddRange(BitConverter.GetBytes(StrName.Length));
            else
                Result.AddRange(BitConverter.GetBytes(0));

            if (StrMessage != null)
                Result.AddRange(BitConverter.GetBytes(StrMessage.Length));
            else
                Result.AddRange(BitConverter.GetBytes(0));

            if (StrName != null)
                Result.AddRange(Encoding.UTF8.GetBytes(StrName));

            if (StrMessage != null)
                Result.AddRange(Encoding.UTF8.GetBytes(StrMessage));

            return Result.ToArray();
        }
        #endregion
    }
}
